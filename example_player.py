import random
import string
import itertools
import string


class Player:
    def __init__(self):
        self.bad_digits = set()
        self.previousGuess = 0
        self.init_guesses = [digit * 4 for digit in string.digits]
    
    def stringify(self, numericGuess):
        return str(numericGuess).rjust(4, "0")

    def guess(self):
        if self.init_guesses:
            return self.init_guesses.pop()

        """Generate a random four-digit string guess for the Mastermind API"""
        guess_digits = list()
            
        self.previousGuess += 1
        while self.invalidGuess(self.stringify(self.previousGuess)):
            self.previousGuess += 1

        guessStr = self.stringify(self.previousGuess)
        return guessStr

    def hasDuplicates(self, guess):
        digits = list(guess)
        uniquedigits = set(digits)
        return len(uniquedigits) < 4

    def hasInvalidDigits(self, guess):
        digits = list(guess)
        for d in digits:
            if d in self.bad_digits:
                return True
        return False

    def invalidGuess(self, guess):
        return self.hasDuplicates(guess) or self.hasInvalidDigits(guess)

    def process_results(self, current_guess, correct_digits, misplaced_digits):
        """Do something useful with the results of the current guess"""
        if correct_digits == 0 and misplaced_digits == 0:
            digits = list(current_guess)
            for d in digits:
                self.bad_digits.add(d)


if __name__ == "__main__":
    player = Player()
    random_guess = player.guess()
    print(f"This Player provides random guesses: {random_guess}")
    print("Run mastermind_example_client.py to use it in the challenge!")
